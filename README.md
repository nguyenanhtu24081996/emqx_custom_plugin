## emqx_custom_plugin

An EMQ X plugin

## Quick Start

### Config
```properties
emqx_custom_plugin.hook.client.connected.1     = {"action": "on_client_connected"}
emqx_custom_plugin.hook.client.disconnected.1  = {"action": "on_client_disconnected"}
emqx_custom_plugin.hook.client.subscribe.1     = {"action": "on_client_subscribe"}
emqx_custom_plugin.hook.client.unsubscribe.1   = {"action": "on_client_unsubscribe"}
emqx_custom_plugin.hook.session.subscribed.1   = {"action": "on_session_subscribed"}
emqx_custom_plugin.hook.session.unsubscribed.1 = {"action": "on_session_unsubscribed"}
emqx_custom_plugin.hook.message.publish.1      = {"action": "on_message_publish"}
emqx_custom_plugin.hook.message.delivered.1    = {"action": "on_message_delivered"}
emqx_custom_plugin.hook.message.acked.1        = {"action": "on_message_acked"}
```

### Notice
some notice....

## License
Apache License Version 2.0

## Author

## Community
- Page: https://www.emax.io
- Other: ....

## Contributors

