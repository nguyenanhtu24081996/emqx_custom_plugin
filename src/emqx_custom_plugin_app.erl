%%%-------------------------------------------------------------------
%% @doc emqx_custom_plugin public API
%% @end
%%%-------------------------------------------------------------------

-module(emqx_custom_plugin_app).

-behaviour(application).

-include("emqx_custom_plugin.hrl").

-emqx_plugin(?MODULE).

-export([ start/2
        , stop/1
        ]).

start(_StartType, _StartArgs) ->
    {ok, Sup} = emqx_custom_plugin_sup:start_link(),
    ?APP:load(),
    ?APP:register_metrics(),
    {ok, Sup}.

stop(_State) ->
    ?APP:unload(),
    ok.
